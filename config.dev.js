const path = require('path');

module.exports = {
    mode: 'production',
    entry: './src/App.js',
    // Loader
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            } 
        ]
    },
    // End Loader
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
}